"""
Custom section for Python-Markdown
==================================

Encapsulate part of text into a div tag with the specified class attribute

This is an adaptation of the Admonition extension present in Python-Markdown itself

License: [MIT](https://opensource.org/licenses/MIT)
"""

import re

from markdown import Extension
from markdown.blockprocessors import BlockProcessor
from markdown.util import etree


class CustomSectionExtension(Extension):
    """ CustomSectionExtension extension for Python-Markdown. 
    """

    def extendMarkdown(self, md, md_globals):
        """ Add CustomSection to Markdown instance. """
        md.registerExtension(self)

        md.parser.blockprocessors.add('custom_section', CustomSectionProcessor(md.parser), '_begin')


class CustomSectionProcessor(BlockProcessor):
    ''' A modified version of [AdmonitionExtension] shipped with Python-Markdown. 
        usage: 
        :my_section:
            bla bla bla
    ''' 

    RE = re.compile(r'(?:^|\n):([\w\-]+): ([\w ]+)(?:\n|$)')
    CLASSNAME = 'section'

    def test(self, parent, block):
        sibling = self.lastChild(parent)
        return self.RE.search(block) or (block.startswith(' ' * self.tab_length) and sibling is not None and
            sibling.get('class', '').find(self.CLASSNAME) != -1)

    def run(self, parent, blocks):
        sibling = self.lastChild(parent)
        block = blocks.pop(0)
        m = self.RE.search(block)

        if m:
            block = block[m.end():]  # removes the first line

        block, theRest = self.detab(block)

        if m:
            klass = m.group(1)
            title = m.group(2)
            div = etree.SubElement(parent, 'div')
            div.set('class', '{} {}'.format(self.CLASSNAME, klass))
            h2title = etree.SubElement(div, 'h2')
            h2title.set('class', '{} {}'.format(self.CLASSNAME, 'title'))
            h2title.text = title
        else:
            div = sibling

        self.parser.parseChunk(div, block)

        # if theRest:
        #     # This block contained unindented line(s) after the first indented
        #     # line. Insert these lines as the first block of the master blocks
        #     # list for future processing.
        #     blocks.insert(0, theRest)


def makeExtension(*args, **kwargs):
    return CustomSectionExtension(*args, **kwargs)
