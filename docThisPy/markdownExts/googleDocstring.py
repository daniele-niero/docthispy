""" 
Google Docstring style for Python-Markdown
====================================

Add Google documentation style support

License: [MIT](https://opensource.org/licenses/MIT)
"""

import re
import textwrap

from markdown import Extension
from markdown.blockprocessors import BlockProcessor
from markdown.preprocessors import Preprocessor
from markdown.util import etree

from .customSection import CustomSectionProcessor


class GoogleDocstringExtension(Extension):
    """ CustomSectionExtension extension for Python-Markdown. """

    def extendMarkdown(self, md, md_globals):
        """ Add CustomSection to Markdown instance. """
        md.registerExtension(self)

        md.preprocessors.add('google_args', ArgsSectionPreprocessor(md.parser), '>html_block')
        md.parser.blockprocessors.add('google_args_section', ArgsSectionProcessor(md.parser), '_begin')


class ArgsSectionProcessor(BlockProcessor):
    ''' Processes Args: section for any function documentation 
    
    The arguments section should look like this:
    ```
    Args:
        param1 (int): The first parameter.
        param2 (Optional[str]): The second parameter. Defaults to None.
            Other lines part of the same description should be indented.

            If a line is separated by a blank line (but still indented)
            it will be inserted in a different paragraph

            ### There is more!
            Other markdown can be used in the section.

        *args: Variable length argument list.
        **kwargs: Arbitrary keyword arguments.
    ```
    ''' 

    RE = re.compile(r"(?:^|\n)(Args):(\s*\n|$)")
    ARGSRE = re.compile(r'^\s{4}(\*{0,2}\w+)\s*(\([\w\s\[\]]*\))?:\s*')

    def __init__(self, parser):
        super(ArgsSectionProcessor, self).__init__(parser)
        self.__table = None
        self.__tbody = None
        self.argDesc = None

    def test(self, parent, block):
        sibling = self.lastChild(parent)
        self.mainMatch = self.RE.search(block)
        self.argMatch  = self.ARGSRE.search(block)
        belongToArgsTable = sibling is not None and sibling.tag=='div' and sibling.get('class')=='section args'
        self.description =  belongToArgsTable and block.startswith(' ' * self.tab_length * 2)
        return self.mainMatch  or (self.argMatch and belongToArgsTable) or self.description

    def run(self, parent, blocks):
        block = blocks.pop(0)
        
        if self.mainMatch:
            klass = self.mainMatch.group(1).lower()
            section = etree.SubElement(parent, 'div')
            section.set('class', 'section args')

            p = etree.SubElement(section, 'p')
            p.text = '<strong>Arguments list</strong>:'

            self.__table = etree.SubElement(section, 'table')
            self.__table.set('class', '{}'.format(klass))
            self.__tbody = etree.SubElement(self.__table, 'tbody')

            # consume the parsed part from the block and insert it back in the blocks array
            block = block[self.mainMatch.end():]
            if block:
                blocks.insert(0, block)


        elif self.argMatch:
            tr = etree.SubElement(self.__tbody, 'tr')

            argDetails = etree.SubElement(tr, 'td')

            argName = etree.SubElement(argDetails, 'span')
            argName.set('class', 'arg_name')
            argName.text = self.argMatch.group(1)
            
            if self.argMatch.group(2):
                argType = etree.SubElement(argDetails, 'span')
                argType.set('class', 'arg_type')
                argType.text = self.argMatch.group(2)

            self.argDesc = etree.SubElement(tr, 'td')
            self.argDesc.set('class', 'arg_description')

            # consume the parsed part from the block and insert it back in the blocks array
            block = block[self.argMatch.end():]
            block = ' '*self.tab_length*2+block

            newBlocks = ['']
            i=0
            for line in block.splitlines():
                print(line)
                if line.startswith(' '*self.tab_length) == False:
                    break
                if self.ARGSRE.search(line):
                    i+=1
                    newBlocks.append('')
                newBlocks[i] += line+'\n'

            i=0
            for block in newBlocks:
                blocks.insert(i, block)
                i+=1

        elif self.description:
            block, rest = self.detab(block)
            block, rest = self.detab(block)
            self.parser.parseChunk(self.argDesc, block)


class ArgsSectionPreprocessor(Preprocessor):
    RE = re.compile(r'(?:^|\n)(Args|Attributes|Returns|Raises|Yields|Examples):(?:\n|$)')

    def run(self, lines):
        newLines=[]
        # i=0
        for line in lines:
            m = self.RE.search(line)
            if m:
                newLines.append(':{}:'.format(m.group(1).lower()))
                newLines.append('    **{}**:  '.format(m.group(1)))
            else:
                newLines.append(line)
            # i+=1 
        return newLines


class ReturnsSectionProcessor(BlockProcessor):
    ''' Processes Returns: section for any function documentation ''' 

    RE = re.compile(r"(?:^|\n)(Returns):(?: *| *\n* {4})([\w|]*):")
    CLASSNAME = 'section return'

    def test(self, parent, block):
        self.match = self.RE.search(block)
        return self.match

    def run(self, parent, block):
        div = etree.SubElement(parent, 'div')
        div.set('class', self.CLASSNAME)

        rtypes = self.match.group(2).split('|')

        p = etree.SubElement(div, 'p')
        p.set('class', 'title')
        s = etree.SubElement(p, 'strong')
        if len(rtypes>1):
            s.text = 'Return types:'
        else:
            s.text = 'Return type:'

        p = etree.SubElement(div, 'p')
        p.set('class', 'return-types')
        for t in rtypes:
            p.text = t + ', '
        p.text = p.text[:-2]

        block





def makeExtension(*args, **kwargs):
    return GoogleDocstringExtension(*args, **kwargs)



