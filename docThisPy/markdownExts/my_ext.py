from __future__ import absolute_import
from __future__ import unicode_literals
from markdown.extensions import Extension
from markdown.inlinepatterns import Pattern, SubstituteTagPattern
from markdown.preprocessors import Preprocessor
from markdown.blockprocessors import BlockProcessor, ListIndentProcessor
from markdown.util import etree

import re


class MyBlockProcessor(BlockProcessor):
    # RE = re.compile('^(\s*):(\w*):$')
    RE = re.compile(r'(?:^|\n):([\w\-]+):(?:\n|$)')
    CLASSNAME = 'section'


    def test(self, parent, block):
        m = bool(self.RE.search(block))
        sibling = self.lastChild(parent)
        print(sibling)
        return self.RE.search(block) or (block.startswith(' ' * self.tab_length) and sibling is not None and
            sibling.get('class', '').find(self.CLASSNAME) != -1)

    def run(self, parent, blocks):
        sibling = self.lastChild(parent)
        block = blocks.pop(0)
        m = self.RE.search(block)

        if m:
            block = block[m.end():]  # removes the first line

        block, theRest = self.detab(block)

        if m:
            klass = m.group(1)
            div = etree.SubElement(parent, 'div')
            div.set('class', '%s %s' % (self.CLASSNAME, klass))
        else:
            div = sibling

        self.parser.parseChunk(div, block)

        if theRest:
            # This block contained unindented line(s) after the first indented
            # line. Insert these lines as the first block of the master blocks
            # list for future processing.
            blocks.insert(0, theRest)


class EnhanceAutoLink(Preprocessor):
    """docstring for EnhanceAutoLink"""
    RegEx = re.compile(r'\s(?!<)([\w-]*://[-A-Z0-9+&@#/%=~_|!:,.;]*)', re.IGNORECASE)

    def run(self, lines):
        i=0
        for line in lines:
            while True:
                m = EnhanceAutoLink.RegEx.search(line)
                if m:
                    start, end = m.start()+1, m.end()
                    line = line[:start]+'<'+line[start:end]+'>'+line[end:]
                else:
                    break
            lines[i] = line
            i+=1
        return lines


class MyPattern(Pattern):
    def handleMatch(self, m):
        div = etree.Element('div')
        div.text = m.group(3)
        return div


class MyExtension(Extension):

    def extendMarkdown(self, md, md_globals):
        # mypattern = MyPattern('(<:)([\w\.]*)(>)')
        # mypattern.md = md
        # md.inlinePatterns.add('mypattern', mypattern, "<not_strong")
        # # for inpat in md.inlinePatterns:
        # #     print(inpat)

        # md.preprocessors.add("enhance_auto_link", EnhanceAutoLink(self), "<normalize_whitespace")
        # for preproc in  md.preprocessors:
        #     print(preproc)

        # md.parser.blockprocessors.add('my_block_processor', MyBlockProcessor(md.parser), '_begin')
        # for key in md.parser.blockprocessors:
        #     print(key)

        pass
        


def makeExtension(*args, **kwargs):
    return MyExtension(*args, **kwargs)
