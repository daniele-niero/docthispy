import os
from .docAst import baseWrappers
from .docAst.docAstVisitor import DocAstVisitor
from .docAst.pyPackage import PyPackage
from .docAst.pyModule import PyModule
from .docAst.pyClass import PyClass

from .template import Template
from .main import HtmlPage


class HtmlDocVisitor(DocAstVisitor):
    def __init__(self, outputPath):
        self.outputPath = outputPath
        self.pages = {}
        self.__backupMKSafe = baseWrappers.Named.markdownSafe

    def start(self, node):
        baseWrappers.Named.markdownSafe = True
        super().start(node)

    def end(self):
        baseWrappers.Named.markdownSafe = self.__backupMKSafe

    def __doPage(self, node):
        page = HtmlPage()

        mkd = Template(node.templateStr).format(node=node)
        mkd += '\n\n---\n\n<pre>\n{}\n</pre>'.format(mkd)

        page.append( mkd )
        self.pages[node] = page

    def __savePage(self, node):
        page = self.pages[node]
        page.toHtmlFile( os.path.join(self.outputPath, node.importStatement.replace('.', '_')+'.html') )
        

    def visit_PyPackage(self, node):
        self.__doPage(node)

    def leave_PyPackage(self, node):
        self.__savePage(node)
       
    def visit_PyModule(self, node):
        self.__doPage(node)

    def leave_PyModule(self, node):
        self.__savePage(node)

    def visit_PyClass(self, node):
        self.__doPage(node)

    def leave_PyClass(self, node):
        self.__savePage(node)

    # def visit_PyFunction(self, node):
    #     self.doPrint(node)


    # def visit_PyMethod(self, node):
    #     self.doPrint(node)

    # def visit_PyAttribute(self, node):
    #     self.doPrint(node)
