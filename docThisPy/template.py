import textwrap
import sys
import copy
import os
import _string


class Template(object):
    ''' complete re-implementation of string.Template '''
    
    def __init__(self, strToFormat, dedent=False):
        if dedent:
            self.strToFormat = textwrap.dedent(strToFormat)
        else:
            self.strToFormat = strToFormat
        self.implicitCount = 0

    def format(self, *args, **kwargs):
        return self._format(self.strToFormat, args, kwargs, 2)

    def _indent(self, strToIndent, indentValue):
        lines = strToIndent.splitlines()
        indentStr = '\n'+' '*indentValue
        return indentStr.join(lines)

    def _format(self, strToFormat, args, kwargs, recursionDepth):
        if recursionDepth < 0:
            raise ValueError('Max string recursion exceeded')
        result = []
        for literalText, fieldName, formatSpec, conversion in self.parse(strToFormat):

            if literalText:
                line = literalText.splitlines()[-1]
                indent = len(line)
                result.append(literalText)
            else:
                indent = 0

            # if there's a field, output it
            if fieldName is not None:
                if fieldName.startswith('for '):
                    _for, itemKey, _in, iterableName = fieldName.split()
                    
                    iterable, argUsed = self.getField(iterableName, args, kwargs)
                    iterator = iter(iterable)
                    item = next(iterator, None)
                    while item:
                        nextItem = next(iterator, None)
                        
                        subKwargs = copy.copy(kwargs)
                        subKwargs[itemKey] = item
                        subResult = self._format(textwrap.dedent(formatSpec).lstrip(), [], subKwargs, recursionDepth)

                        if nextItem:
                            subResult+='\n'
                        result.append(self._indent(subResult, indent))

                        item = nextItem


                elif fieldName.startswith('if '):
                    _if, boolName = fieldName.split()
                    boolVal, argUsed = self.getField(boolName, args, kwargs)
                    if boolVal:
                        # subKwargs = copy.copy(kwargs)
                        # subKwargs[itemKey] = item
                        subResult = self._format(textwrap.dedent(formatSpec).lstrip(), [], kwargs, recursionDepth)
                        result.append(self._indent(subResult, indent))

                else:
                    # given the fieldName, find the object it references and the argument it came from
                    obj, argUsed = self.getField(fieldName, args, kwargs)

                    # do any conversion on the resulting object
                    obj = self.convertField(obj, conversion)

                    # expand the format spec, if needed
                    formatSpec = self._format(formatSpec, args, kwargs, recursionDepth-1)

                    # format the object and append to the result
                    result.append(self._indent(self.formatField(obj, formatSpec), indent))

        return ''.join(result)

    def getValue(self, key, args, kwargs):
        if key not in kwargs:
            return ''
        elif key=='':
            key = self.implicitCount
            self.implicitCount += 1
        elif isinstance(key, int):
            return args[key]
        else:
            return kwargs[key]

    def formatField(self, value, formatSpec):
        return format(value, formatSpec)

    def convertField(self, value, conversion):
        # do any conversion on the resulting object
        if conversion is None:
            return value
        elif conversion == 's':
            return str(value)
        elif conversion == 'r':
            return repr(value)
        elif conversion == 'a':
            return ascii(value)
        raise ValueError("Unknown conversion specifier {0!s}".format(conversion))

    def parse(self, formatString):
        ''' returns an iterable that contains tuples of the form:
            (literalText, fieldName, formatSpec, conversion)
            literalText can be zero length
            fieldName can be None, in which case there's no
             object to format and output
            if fieldName is not None, it is looked up, formatted
             with formatSpec and conversion and then used '''
        return _string.formatter_parser(formatString)

    def getField(self, fieldName, args, kwargs):
        ''' given a fieldName, find the object it references.

        Args:
            fieldName (str): the field being looked up, e.g. "0.name" or "lookup[3]" or simple "something"
            args, kwargs: as passed in to vformat 
        '''
        first, rest = _string.formatter_field_name_split(fieldName)
        obj = self.getValue(first, args, kwargs)

        # loop through the rest of the fieldName, doing getattr or getitem as needed
        for isAttr, i in rest:
            if isAttr:
                obj = getattr(obj, i)
            else:
                obj = obj[i]

        return obj, first


class Templateable:
    TEMPLATES_PATH = os.path.join(os.path.split(__file__)[0], '../templates')

    def __init__(self, ext='md'):
        '''
        Args:
            ext (str): template file's extension '''
        self.extension = ext

    @property
    def exists(self):
        return self._exists
    

    @property
    def templatesPath(self):
        return Templateable.TEMPLATES_PATH

    @templatesPath.setter
    def templatesPath(self, newPath):
        Templateable.TEMPLATES_PATH = os.path.abspath(os.path.expandvars(newPath))
    
    @property    
    def templateStr(self):
        className = self.__class__.__name__
        templatePath = os.path.join(self.templatesPath, '{}.{}'.format(className, self.extension))
        if os.path.exists(templatePath):
            templateFile = open(templatePath, 'r')
            templateResult = templateFile.read()
            templateFile.close()
            return templateResult
        else:
            return ''

