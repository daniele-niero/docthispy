import ast
import sys

from .baseWrappers import Named, Documentable


class PyFunction(Documentable, Named):
    def __init__(self, astFunctionDef, parentDocAST=None):
        super(PyFunction, self).__init__(astFunctionDef, parentDocAST)
        Named.__init__(self, astFunctionDef.name)

        self.__args = [arg.arg for arg in astFunctionDef.args.args]
        self.__defaults = [self.__formatDefaultArgumentValue(d) for d in astFunctionDef.args.defaults]
        self.__kwds = astFunctionDef.args.kwarg
        self.__varg = astFunctionDef.args.vararg 

    def __formatDefaultArgumentValue(self, astInstance):
        value = ''

        if isinstance(astInstance, ast.NameConstant):
            value = astInstance.value
        elif isinstance(astInstance, ast.Str):
            value = '"{}"'.format(astInstance.s)
        elif isinstance(astInstance, ast.Num):
            value = str(astInstance.n)
        elif isinstance(astInstance, ast.Name):
            value = astInstance.id
        elif isinstance(astInstance, ast.Attribute):
            value = '{}.{}'.format(self.__formatDefaultArgumentValue(astInstance.value), astInstance.attr)
        elif isinstance(astInstance, ast.Tuple):
            value = '('
            for element in astInstance.elts:
                value += self.__formatDefaultArgumentValue(element)
                value += ', '
            value = value[:-2]+')'
        elif isinstance(astInstance, ast.List):
            value = '['
            for element in astInstance.elts:
                value += self.__formatDefaultArgumentValue(element)
                value += ', '
            value = value[:-2]+']'
        elif isinstance(astInstance, ast.Call):
            func = self.__formatDefaultArgumentValue(astInstance.func)
            args = [self.__formatDefaultArgumentValue(arg) for arg in astInstance.args]
            value = '{}({})'.format(func, ', '.join(args))
        return value

    def getMandatoryArgs(self):
        toIndex = len(self.__args)-len(self.__defaults)
        for i in range(0, toIndex):
            yield self.__args[i]
    mandatoryArgs = property(getMandatoryArgs)

    def getOptionalArgs(self, withValue=False):
        offsetIndex = len(self.__args)-len(self.__defaults)
        if not withValue:
            for i in range(offsetIndex, len(self.__args)):
                yield self.__args[i]
        else:
            for i in range(0, len(self.__defaults)):
                yield '{}={}'.format(self.__args[i+offsetIndex], self.__defaults[i])
    optionalArgs = property(getOptionalArgs)

    def getSignature(self, withDefaultValues=True):        
        varg = []
        if self.__varg:
            varg = ['*{}'.format(self.__varg)]

        kwds = []
        if self.__kwds:
            kwds = ['**{}'.format(self.__kwds)]

        args = []
        for arg in self.getMandatoryArgs():
            args.append(arg)
        for arg in self.getOptionalArgs(withDefaultValues):
            args.append(arg)
        args += varg
        args += kwds

        argumentString = ', '.join(args)
        return '{}({})'.format(self.name, argumentString)
    signature = property(getSignature)


class PyMethod(PyFunction):
    def __init__(self, astFunctionDef, parentDocAST=None):
        super().__init__(astFunctionDef, parentDocAST)

        self.__selfArg = self._PyFunction__args[0]
        self._PyFunction__args.pop(0)
