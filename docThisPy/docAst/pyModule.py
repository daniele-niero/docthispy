import ast
import os

from . import utilities
from .baseWrappers import Named, Documentable
from .pyFunction import PyFunction
from .pyClass import PyClass


class PyModule(Documentable, Named):
    def __init__(self, filePath, parentAstDoc=None):
        self.__filePath = filePath

        with open(self.__filePath) as fd:
            content = fd.read()

        astInstance = ast.parse(content)
        super(PyModule, self).__init__(astInstance, parentAstDoc)
        Named.__init__(self,  os.path.splitext(os.path.basename(self.__filePath))[0])

        self.__classes      = [PyClass(node, self) for node in astInstance.body if isinstance(node, ast.ClassDef)]
        self.__functions    = [PyFunction(node, self) for node in astInstance.body if isinstance(node, ast.FunctionDef)]

        self.__attributes = utilities.findAttributes(self, astInstance.body, private=False)

    @property
    def classes(self):
        for klass in self.__classes:
            yield klass
    
    @property
    def hasClasses(self):
        return len(self.__classes)
    
    @property
    def functions(self):
        for function in self.__functions:
            yield function
        
    @property
    def hasFunctions(self):
        return len(self.__functions)


    @property
    def attributes(self):
        for attr in self.__attributes:
            yield attr
            
    @property
    def hasAttributes(self):
        return len(self.__attributes)
    


