'''
Common classes used in all the docAst nodes  
'''

import json
import ast
import os

from ..template import Templateable


class DocASTBase:
    def __init__(self, astInstance, parentDocAST=None):
        self.__children = []

        if parentDocAST != None:
            if isinstance(parentDocAST, DocASTBase):
                self.__parent = parentDocAST
            else:
                self.__parent = DocASTBase(parentDocAST, None)
            self.__parent._addChild(self)
        else:
            self.__parent = None

    def test(self, cccio, ciaccio='hoi'):
        pass

    def _addChild(self, childDocAst):
        self.__children.append(childDocAst)

    def iterChildren(self):
        for child in self.__children:
            yield child

    @property
    def parent(self):
        return self.__parent

    @property
    def importStatement(self):
        def collect(pack, coll):
            if pack.parent:
                collect(pack.parent, coll)
            coll.append(pack.name)
            
        packs = []
        collect(self, packs)
        return '.'.join(packs)

    @property
    def linkReference(self):
        return self.importStatement.replace('.', '_')


class Named:
    markdownSafe = False

    def __init__(self, name):
        self.__name = name

    @property
    def name(self):
        if Named.markdownSafe:
            if self.__name.startswith('_'):
                return '\\'+self.__name
            else:
                return self.__name
        else:
            return self.__name

    def __repr__(self):
        return '<{}>{}'.format(self.__class__.__name__, self.name)


class Documentable(DocASTBase, Templateable):
    def __init__(self, astInstance, parentDocAST=None):
        DocASTBase.__init__(self, astInstance, parentDocAST)
        Templateable.__init__(self)

        self.__docstring = ''
        if isinstance(astInstance, (ast.Module, ast.ClassDef, ast.FunctionDef)):
            self.__docstring = ast.get_docstring(astInstance)
        if not self.__docstring:
            self.__docstring = '' # force it to be always a string

    @property
    def doc(self):
        return self.__docstring

    @property
    def brief(self):
        lines = self.__docstring.splitlines()
        if not lines:
            return ''
        else:
            for line in lines:
                if line:
                    return line

    def appendDocString(self, doc):
        if not doc:
            return
        self.__docstring += '\n\n' +doc

    def prependDocString(self, doc):
        if not doc:
            return
        self.__docstring = doc + '\n\n' +self.__docstring
