
class DocAstVisitor:
    """
    A node visitor base class that walks the abstract syntax tree and calls a
    visitor function for every node found.  This function may return a value
    which is forwarded by the `visit` method.

    This class is meant to be subclassed, with the subclass adding visitor
    methods.

    Per default the visitor functions for the nodes are ``'visit_'`` +
    class name of the node.  So a `TryFinally` node visit function would
    be `visit_TryFinally`.  This behavior can be changed by overriding
    the `visit` method.  If no visitor function exists for a node
    (return value `None`) the `generic_visit` visitor is used instead.
    """

    def visit(self, node):
        """Visit a node."""
        methodName = 'visit_' + node.__class__.__name__
        visitorMethod = getattr(self, methodName, None)
        if visitorMethod:
            visitorMethod(node)
        for child in node.iterChildren():
            self.visit(child)
        self.leave(node)

    def leave(self, node):
        method = 'leave_' + node.__class__.__name__
        # for child in node.iterChildren():
        #     self.leave(child)
        leaver = getattr(self, method, None)
        if leaver:
            leaver(node)

    def start(self, node):
        self.visit(node)
        self.end()

    def end(self):
        pass

