import ast
import sys

from .baseWrappers import Named, Documentable


class PyFunction(Documentable, Named):
    def __init__(self, astFunctionDef, parentDocAST=None):
        super(PyFunction, self).__init__(astFunctionDef, parentDocAST)
        Named.__init__(self, astFunctionDef.name)

        if sys.version_info[0] < 3:
            self.__args = [arg.id for arg in astFunctionDef.args.args]
        else:
            self.__args = [arg.arg for arg in astFunctionDef.args.args]
        self.__defaults = [self.__formatDefaultArgumentValue(d) for d in astFunctionDef.args.defaults]
        self.__kwds = astFunctionDef.args.kwarg
        self.__varg = astFunctionDef.args.vararg 

    def __formatDefaultArgumentValue(self, astInstance):
        value = ''

        if isinstance(astInstance, ast.Str):
            value = astInstance.s
        elif isinstance(astInstance, ast.Num):
            value = str(astInstance.n)
        elif isinstance(astInstance, ast.Name):
            value = astInstance.id
        elif isinstance(astInstance, ast.Attribute):
            value = '{}.{}'.format(self.__formatDefaultArgumentValue(astInstance.value), astInstance.attr)
        elif isinstance(astInstance, ast.Tuple):
            value = '('
            for element in astInstance.elts:
                value += self.__formatDefaultArgumentValue(element)
                value += ', '
            value = value[:-2]+')'
        elif isinstance(astInstance, ast.List):
            value = '['
            for element in astInstance.elts:
                value += self.__formatDefaultArgumentValue(element)
                value += ', '
            value = value[:-2]+']'
        elif isinstance(astInstance, ast.Call):
            func = self.__formatDefaultArgumentValue(astInstance.func)
            args = [self.__formatDefaultArgumentValue(arg) for arg in astInstance.args]
            value = '{}({})'.format(func, ', '.join(args))
        return value

    # def getMandatoryArgs(self):
    #     toIndex   = len(self.__args)-len(self.__defaults)
    #     return [self.__args[i] for i in range(0, toIndex)]

    # def getOptionalArgs(self, withValue=False):
    #     offsetIndex = len(self.__args)-len(self.__defaults)
    #     if not withValue:
    #         return [self.__args[i] for i in range(offsetIndex, len(self.__args))]
    #     else:
    #         return ['{}={}'.format(self.__args[i+offsetIndex], self.__defaults[i]) for i in range(0, len(self.__defaults))]

    # def getSignature(self, withDefaultValues=True):        
    #     varg = []
    #     if self.__varg:
    #         varg = ['*{}'.format(self.__varg)]

    #     kwds = []
    #     if self.__kwds:
    #         kwds = ['**{}'.format(self.__kwds)]

    #     argumentString = ', '.join(self.getMandatoryArgs() +
    #                                self.getOptionalArgs(withDefaultValues) +
    #                                varg +
    #                                kwds)

    #     return '{}({})'.format(self.name, argumentString)


class PyMethod(PyFunction):
    pass