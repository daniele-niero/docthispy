import ast
import os

from . import utilities
from .baseWrappers import Named, Documentable
from .pyFunction import PyFunction
from .pyClass import PyClass


class PyModule(Documentable, Named):
    def __init__(self, filePath, parentAstDoc=None):
        self.__filePath = filePath

        with open(self.__filePath) as fd:
            content = fd.read()

        astInstance = ast.parse(content)
        super(PyModule, self).__init__(astInstance, parentAstDoc)
        Named.__init__(self,  os.path.splitext(os.path.basename(self.__filePath))[0])

        self.__classes      = [PyClass(node, self) for node in astInstance.body if isinstance(node, ast.ClassDef)]
        self.__functions    = [PyFunction(node, self) for node in astInstance.body if isinstance(node, ast.FunctionDef)]

        self.__attributes = utilities.findAttributes(self, astInstance.body, private=False)


