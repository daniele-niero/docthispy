'''
Common classes used in all the docAst nodes  
'''

import json
import ast
import os


class DocASTBase(object):
    def __init__(self, astInstance, parentDocAST=None):
        self.__children = []

        if parentDocAST != None:
            if isinstance(parentDocAST, DocASTBase):
                self.__parent = parentDocAST
            else:
                self.__parent = DocASTBase(parentDocAST, None)
            self.__parent._addChild(self)
        else:
            self.__parent = None

    def _addChild(self, childDocAst):
        self.__children.append(childDocAst)

    # def iterChildren(self):
    #     for child in self.__children:
    #         yield child

    # def _getParent(self):
    #     return self.__parent
    # parent = property(_getParent)

    # def getImportStatement(node):

    #     def collect(pack, coll):
    #         if pack.parent:
    #             collect(pack.parent, coll)
    #         coll.append(pack.name)
            
    #     packs = []
    #     collect(node, packs)
    #     return '.'.join(packs)

    # @property
    # def linkReference(self):
    #     return self.getImportStatement().replace('.', '_')



class Named(object):
    markdownSafe = False

    def __init__(self, name):
        self.__name = name

    @property
    def name(self):
        if Named.markdownSafe:
            if self.__name.startswith('_'):
                return '\\'+self.__name
            else:
                return self.__name
        else:
            return self.__name


class Documentable(DocASTBase):
    def __init__(self, astInstance, parentDocAST=None):
        super(Documentable, self).__init__(astInstance, parentDocAST)

        self.__docstring = ''
        if isinstance(astInstance, (ast.Module, ast.ClassDef, ast.FunctionDef)):
            self.__docstring = ast.get_docstring(astInstance)
        if not self.__docstring:
            self.__docstring = '' # force it to be always a string

        self.templatesPath = os.path.join(os.path.split(__file__)[0], '../../templates')

    @property
    def doc(self):
        return self.__docstring

    # @property
    # def brief(self):
    #     lines = self.__docstring.splitlines()
    #     if lines:
    #         return lines[0]
    #     else:
    #         return ''

    # @property
    # def templatesPath(self):
    #     return self.__templatesPath

    # @templatesPath.setter
    # def templatesPath(self, newPath):
    #     self.__templatesPath = os.path.abspath(os.path.expandvars(newPath))
    
    # @property    
    # def templateStr(self):
    #     className = self.__class__.__name__
    #     templateFile = open(os.path.join(self.templatesPath, className+'.md'), 'r')
    #     templateResult = templateFile.read()
    #     templateFile.close()
    #     return templateResult

    def appendDocString(self, doc):
        if not doc:
            return
        self.__docstring += '\n\n' +doc

    # def prependDocString(self, doc):
    #     if not doc:
    #         return
    #     self.__docstring = doc + '\n\n' +self.__docstring
