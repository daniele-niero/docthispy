import ast

from .baseWrappers import *


class PyAttribute(Documentable, Named):
    def __init__(self, astAssign, parentDocAST=None):
        '''
        Args:
            astAssign (ast.Assign): An ast.Assign but it must contain only one element
                in its targets list and this element must be of type ast.Name or ast.Attribute.  
                Multiple targets or targets of type ast.Tuple, are not allowed becasue PyAttribute 
                is for holding information about one single attribute (target).
        '''
        super(PyAttribute, self).__init__(astAssign, parentDocAST)

        name = ''
        if isinstance(astAssign.targets[0], ast.Name):
            name = astAssign.targets[0].id
        elif isinstance(astAssign.targets[0], ast.Attribute):
            name = astAssign.targets[0].attr
        Named.__init__(self, name)
