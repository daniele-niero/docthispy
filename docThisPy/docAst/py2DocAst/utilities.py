import ast
import inspect

def findAttributes(this, where, private=False):
    result = []
    alreadyAssigned = []

    def wrap(target, value, name, doc):
        from . import pyAttribute
        if private and name.startswith('_')==False:
            return
        if not private and name.startswith('_'):
            return
        
        alreadyAssigned.append(name)
        a = ast.Assign([target], value)
        p = pyAttribute.PyAttribute(a, this)
        p.appendDocString(doc)
        result.append(p)

    assignes = [node for node in where if isinstance(node, ast.Assign)]
    for assign in assignes:
        # search for possible documentation
        id = where.index(assign)
        doc = ''
        if id>0:
            docNode = where[id-1]
            if isinstance(docNode, ast.Expr) and isinstance(docNode.value, ast.Str):
                doc = inspect.cleandoc(docNode.value.s)
        
        # Instead of having multiple targets per assign, create an assign per target
        for target in assign.targets:
            if isinstance(target, ast.Tuple):
                for ttarget in target.elts:
                    if isinstance(target, ast.Name):
                        targetId = target.id
                    elif isinstance(target, ast.Attribute):
                        targetId = target.attr
                    wrap(ttarget, assign.value, targetId, doc)
            else:
                targetId = ''
                if isinstance(target, ast.Name):
                    targetId = target.id
                elif isinstance(target, ast.Attribute):
                    targetId = target.attr

                wrap(target, assign.value, targetId, doc)

    return result
