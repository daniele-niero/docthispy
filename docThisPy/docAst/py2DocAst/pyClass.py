import ast

from .utilities import findAttributes
from .baseWrappers import Named, Documentable
from .pyFunction import PyMethod


class PyClass(Documentable, Named):
    def __init__(self, astClassDef, parentDocAST=None):
        super(PyClass, self).__init__(astClassDef, parentDocAST)
        Named.__init__(self, astClassDef.name)

        self.__methods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and not node.name.startswith('_')]
        self.__staticMethods = []
        self.__initMethod = None
        initAstNode = None
        for node in astClassDef.body:
            if isinstance(node, ast.FunctionDef) and node.name==('__init__'):
                initAstNode = node
                self.__initMethod = PyMethod(node, None) 
        self.__specialMethods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and node.name!=('__init__') and node.name.startswith('__') and node.name.endswith('__')]
        self.__privateMethods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and node.name[0]=='_' and node.name[1]!='_']

        self.__staticAttributes = findAttributes(self, astClassDef.body)
        if self.__initMethod:
            self.__attributes = findAttributes(self, initAstNode.body)
            self.__privateAttributes = findAttributes(self, initAstNode.body, True)
        else:
            self.__attributes = []
            self.__privateAttributes = []

        if self.__initMethod:
            self.appendDocString(self.__initMethod.doc)