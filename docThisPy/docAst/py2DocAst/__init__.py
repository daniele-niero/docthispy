import gzip
import sys
import glib
from os import path
import jsonpickle


if __name__ == '__main__':
    from docThisPy.docAst.pyPackage import PyPackage
    
    pack = PyPackage(sys.argv[1])
    exchangePath = path.join(sys.argv[2], 'exchangeDocAstData.bin')
    with glib.open(exchangePath, 'wb') as pickleFile:
        pickleFile.write(jsonpickle.encode(pack))