import ast

from .utilities import findAttributes
from .baseWrappers import Named, Documentable
from .pyFunction import PyMethod


class PyClass(Documentable, Named):
    def __init__(self, astClassDef, parentDocAST=None):
        super(PyClass, self).__init__(astClassDef, parentDocAST)
        Named.__init__(self, astClassDef.name)

        self.__methods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and not node.name.startswith('_')]
        self.__staticMethods = []
        self.__initMethod = None
        initAstNode = None
        for node in astClassDef.body:
            if isinstance(node, ast.FunctionDef) and node.name==('__init__'):
                initAstNode = node
                self.__initMethod = PyMethod(node, None) 
        self.__specialMethods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and node.name!=('__init__') and node.name.startswith('__') and node.name.endswith('__')]
        self.__privateMethods = [PyMethod(node, self) for node in astClassDef.body if isinstance(node, ast.FunctionDef) and node.name[0]=='_' and node.name[1]!='_']

        self.__staticAttributes = findAttributes(self, astClassDef.body)
        if self.__initMethod:
            self.__attributes = findAttributes(self, initAstNode.body)
            self.__privateAttributes = findAttributes(self, initAstNode.body, True)
        else:
            self.__attributes = []
            self.__privateAttributes = []

        if self.__initMethod:
            self.appendDocString(self.__initMethod.doc)

    @property
    def signature(self):
        if self.__initMethod:
            if Named.markdownSafe:
                return self.__initMethod.getSignature().replace('\\__init__', self.name)
            else:
                return self.__initMethod.getSignature().replace('\\__init__', self.name)
        else:
            return '{}()'.format(self.name)

    @property
    def publicMethods(self):
        for meth in self.__methods:
            yield meth

    @property
    def hasPublicMethods(self):
        return len(self.__methods)
    

    @property
    def privateMethods(self):
        for meth in self.__privateMethods:
            yield meth

    @property
    def hasPrivateMethods(self):
        return len(self.__privateMethods)


    @property
    def specialMethods(self):
        for meth in self.__specialMethods:
            yield meth

    @property
    def hasSpecialMethods(self):
        return len(self.__specialMethods)


    @property
    def initMethod(self):
        return self.__initMethod

    @property
    def staticAttributes(self):
        for attr in self.__staticAttributes:
            yield attr

    @property
    def publicAttributes(self):
        for attr in self.__attributes:
            yield attr

    @property
    def privateAttributes(self):
        for attr in self.__privateAttributes:
            yield attr