import ast
import os
from glob import glob
import textwrap

from . import utilities
from .baseWrappers import Documentable, Named
from .pyModule import PyModule

from ..template import Template


class PyPackage(Documentable, Named):
    def __init__(self, packagePath, parentAstDoc=None):
        assert glob(os.path.join(packagePath, '__init__.py')), 'This is not a valid package {}'.format(packagePath)

        super().__init__(packagePath, parentAstDoc)
        if parentAstDoc and not isinstance(parentAstDoc, PyPackage):
            raise RuntimeError('PyPackage can only be child of another PyPackage')

        if parentAstDoc:
            self.__relativePath = os.path.relpath(packagePath, parentAstDoc.getFullPath())
        else:
            self.__relativePath = packagePath

        Named.__init__(self, os.path.basename(self.__relativePath))

        self.__initModule = None
        self.__modules = []
        self.__subPackages = []
        for f in os.listdir(packagePath):
            p = os.path.join(packagePath, f)
            if os.path.isfile(p) and f.endswith('.py'):
                if f=='__init__.py':
                    self.__initModule = PyModule(p, None)
                else:
                    self.__modules.append( PyModule(p, self) )
            elif os.path.isdir(p):
                if glob(os.path.join(p, '__init__.py')):
                    self.__subPackages.append(PyPackage(p, self))

        self.appendDocString(self.__initModule.doc)

    def getInitModule(self):
        return self.__initModule

    @property
    def modules(self):
        for module in self.__modules:
            yield module

    @property
    def hasModules(self):
        return len(self.__modules)
    
    @property
    def subPackages(self):
        for pack in self.__subPackages:
            yield pack

    @property
    def hasSubPackages(self):
        return len(self.__subPackages)
    

    def getRelativePath(self):
        return self.__relativePath

    def getFullPath(self):
        if self.parent:
            return os.path.join(self.parent.getFullPath(), self.__relativePath)
        else:
            return self.__relativePath

    # def toMarkdown(self):
    #     backupValue = Named.markdownSafe
    #     Named.markdownSafe = True

    #     mkd = Template(self.templateStr, False)
    #     final = mkd.format(package = self)

    #     Named.markdownSafe = backupValue

    #     return final
