import re
from mistletoe.block_token import BlockToken
from mistletoe import HTMLRenderer, Document
from mistletoe.ast_renderer import ASTRenderer
from mistletoe.block_token import tokenize


class WrappedSection(BlockToken):
    OPEN_RE = re.compile(r'^\s*:::\s([ \w]+)?( <(\w+)>)?( \{([ \w-]+)\})?[\r\n]+')
    CLOSE_RE = re.compile(r'^\s*:::[\r\n]+')

    def __init__(self, lines):
        match_obj = WrappedSection.OPEN_RE.match(lines[0])
        self.title = match_obj.group(1)
        self.tag = match_obj.group(3) if match_obj.group(3) else 'section'
        self.classes = ' '+match_obj.group(5) if match_obj.group(5) else ''
        self.children = tokenize(lines[1:-1])

    @classmethod
    def start(cls, line):
        match_obj = cls.OPEN_RE.match(line)
        return match_obj

    @classmethod
    def read(cls, lines):
        line_buffer = [next(lines)]
        next_line = lines.peek()

        other_opens = 0
        while True:
            match_close = cls.CLOSE_RE.match(next_line)
            if match_close:
                if other_opens==0:
                    line_buffer.append(next(lines))
                    break
                else:
                    other_opens -= 1

            match_open = cls.OPEN_RE.match(next_line)
            if match_open:
                other_opens += 1


            line_buffer.append(next(lines))
            next_line = lines.peek()
            if not next_line:
                break

        return line_buffer


def visit_wrapped_section(cls, token):
    cls.stack.append('\n<{token.tag} class="block-section{token.classes}">'.format(token=token))
    if token.title:
        cls.stack.append('\n<h1 class="block-section-title{token.classes}">{token.title}</h1>\n'.format(token=token))

def leave_wrapped_section(cls, token):
    cls.stack.append('\n</{.tag}>\n'.format(token))


# def make_extension(renderer):
#     renderer.register_token(WrappedSection, render_wrapped_section)



# mk = '''
# !:: Miao

# !:: pino div

# CAZZOLO

# !::

# miao
# !::
# '''


# # # print (render_wrapped_section.__name__)

# with HTMLRenderer() as renderer:
#     renderer.register_token(WrappedSection, render_wrapped_section)
#     print(renderer.render(Document(mk)))