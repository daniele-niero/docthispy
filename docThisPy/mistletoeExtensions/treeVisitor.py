import re
import inspect
from mistletoe import block_token, span_token


class TreeVisitor:
    extesion_functions_map = {}

    def __enter__(self):
        """
        Make visitor classes into context managers.
        """
        return self

    def __exit__(self, exception_type, exception_val, traceback):
        """
        Make visitor classes into context managers.

        Reset block_token._token_types and span_token._token_types.
        """
        block_token.reset_tokens()
        span_token.reset_tokens()

    def _token_to_func(self, token_cls):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', token_cls.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def visit(self, token):
        token_cls_name = token.__class__.__name__ 

        # give precedence to subclass visit methods, if not found, 
        # try if it's an extension with an external function registered
        methodName = 'visit_' + self._token_to_func(token)
        visitorMethod = getattr(self, methodName, None)
        if visitorMethod:
            visitorMethod(token)
        elif token_cls_name in TreeVisitor.extesion_functions_map.keys() and \
             TreeVisitor.extesion_functions_map[token_cls_name][0]:
            TreeVisitor.extesion_functions_map[token_cls_name][0](self, token)
        else:
            self.visit_default(token)

    def visit_default(self, token):
        pass

    def leave(self, token):
        token_cls_name = token.__class__.__name__ 
        method = 'leave_' + self._token_to_func(token)
        leaver = getattr(self, method, None)
        if leaver:
            leaver(token)
        elif token_cls_name in TreeVisitor.extesion_functions_map.keys() and \
             TreeVisitor.extesion_functions_map[token_cls_name][1]:
            TreeVisitor.extesion_functions_map[token_cls_name][1](self, token)

    def walk(self, token):
        def inner_walk(token):
            self.visit(token)

            if hasattr(token, 'children'):
                for child in token.children:
                    inner_walk(child)

            self.leave(token)
        inner_walk(token)

        return self.end_walk()

    def end_walk(self):
        pass

    @staticmethod
    def register_token(token_class, visit_function_ptr=None, leave_function_ptr=None):
        inspect.getmodule(token_class.__bases__[0]).add_token(token_class)
        TreeVisitor.extesion_functions_map[token_class.__name__] = (visit_function_ptr, leave_function_ptr)

    @staticmethod
    def _tokens_from_module(module):
        """
        Helper method; takes a module and returns a list of all token classes
        specified in module.__all__. Useful when custom tokens are defined in a
        separate module.
        """
        return [getattr(module, name) for name in module.__all__]



import sys
from itertools import chain
from urllib.parse import quote
from mistletoe.base_renderer import BaseRenderer
if sys.version_info < (3, 4):
    from mistletoe import _html as html
else:
    import html

from dominate import tags
tags.strong.is_inline = True

class DummyTag(tags.html_tag):

    def _render(self, sb, indent_level, indent_str, pretty, xhtml):
        indent_str = ''
        inline = self._render_children(sb, indent_level + 1, indent_str, pretty, xhtml)

        if pretty and not inline:
            sb.append('\n')
            sb.append(indent_str * indent_level)

        return sb


class br(tags.br):
    is_single = True
    is_inline = False

    def __init__(self, *args, is_hard=False, **kwargs):
        super().__init__(*args, **kwargs)
        self.is_hard = is_hard

    def _render(self, sb, indent_level, indent_str, pretty, xhtml):
        print (sb, indent_level, indent_str, pretty, xhtml)
        sb[-2] = ''
        sb[-1] = ''
        rendered = super()._render(sb, indent_level=2, indent_str='  ', pretty=True, xhtml=self.is_hard)
        # rendered[-4] = ''
        # rendered[-5] = ''
        rendered[-1] += '\n'
        print (rendered)

        return rendered



class HTMLRenderer2(TreeVisitor):
    def __init__(self):
        super().__init__()
        # self.stack = []
        self.active_tag = DummyTag()

        # for btoken in TreeVisitor._tokens_from_module(block_token):
        #     self.register_token(btoken)

        # for stoken in TreeVisitor._tokens_from_module(span_token):
        #     self.register_token(stoken)            

        self.register_token(block_token.HTMLBlock)
        self.register_token(span_token.HTMLSpan)
        
        # html.entities.html5 includes entitydefs not ending with ';',
        # CommonMark seems to hate them, so...
        self._stdlib_charref = html._charref
        _charref = re.compile(r'&(#[0-9]+;'
                              r'|#[xX][0-9a-fA-F]+;'
                              r'|[^\t\n\f <&#;]{1,32};)')
        html._charref = _charref

    def visit(self, token):
        if isinstance(token, block_token.BlockToken):
            self.in_block = True
        else:
            self.in_block = False

        super().visit(token)


    @staticmethod
    def escape_html(raw):
        return html.escape(html.unescape(raw)).replace('&#x27;', "'")

    def __exit__(self, exception_type, exception_val, traceback):
        super().__exit__(exception_type, exception_val, traceback)
        html._charref = self._stdlib_charref

    def visit_raw_text(self, token):
        # print(repr(token.content))
        self.active_tag.add_raw_string(self.escape_html(token.content))
        print(token.content, self.active_tag.get())

    def visit_line_break(self, token):
        if token.soft:
            self.active_tag.add_raw_string('soft\n')
        else:
            self.active_tag.add_raw_string('<br />\n')

    def visit_strong(self, token):
        parent_tag = self.active_tag
        self.active_tag = parent_tag.add(tags.strong())

    def visit_emphasis(self, token):
        parent_tag = self.active_tag
        self.active_tag = parent_tag.add(tags.em()) 

    def visit_inline_code(self, token):
        parent_tag = self.active_tag
        self.active_tag = parent_tag.add(tags.code())

    def visit_strikethrough(self, token):
        parent_tag = self.active_tag
        self.active_tag = parent_tag.add(tags.del_())

    def visit_image(self, token):
        parent_tag = self.active_tag
        print ('&&&&', token.src)
        self.active_tag = parent_tag.add(tags.img(src=token.src))
        self.active_tag.xhtml = True        

    def leave(self, token):
        super().leave(token)
        if self.active_tag and self.active_tag.parentNode:
            self.active_tag = self.active_tag.parentNode

    def end_walk(self):
        # return ''.join(self.stack)
        print('--------------------------------')
        print (self.active_tag.render(xhtml=True, pretty=True))



from mistletoe import Document
from docThisPy.mistletoeExtensions import wrappedSection

# HTMLRenderer.register_token(wrappedSection.WrappedSection, wrappedSection.visit_wrapped_section, wrappedSection.leave_wrapped_section)


# inspect.getmodule(WrappedSection.__bases__[0]).add_token(WrappedSection)


mk = '''
## ciao

bla bla bla  
bla *miao*

pre **aiuto mamma mia** post
other line

    cicoo
    crioo
    togotgo

![foo](/url "title")

::: Suka {forte}
# yeppa!
:::

'''

with HTMLRenderer2() as tv:
    print (tv.walk(Document(mk)))


# from mistletoe.html_renderer import HTMLRenderer
# with HTMLRenderer() as tv:
#     print (tv.render(Document(mk)))