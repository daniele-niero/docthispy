import re
from urllib.parse import quote
from mistletoe import block_token, span_token
from .treeVisitor import TreeVisitor

import sys
if sys.version_info < (3, 4):
    from mistletoe import _html as html
else:
    import html


class HTMLRenderer(TreeVisitor):
    def __init__(self):
        super().__init__()

        self.indent = -1
        self._suppress_ptag_stack = [False]

        self.register_token(block_token.HTMLBlock)
        self.register_token(span_token.HTMLSpan)

        # html.entities.html5 includes entitydefs not ending with ';',
        # CommonMark seems to hate them, so...
        self._stdlib_charref = html._charref
        _charref = re.compile(r'&(#[0-9]+;'
                              r'|#[xX][0-9a-fA-F]+;'
                              r'|[^\t\n\f <&#;]{1,32};)')
        html._charref = _charref


    def leave(self, token):
        breakline = ''
        if (isinstance(token, block_token.Paragraph) and not self._suppress_ptag_stack[-1]) or \
            isinstance(token, block_token.BlockToken):
            breakline = '\n'
        result = super().leave(token)
        return result + breakline if result else ''

    def walk(self, token):
        collected_results = [entry for entry in super().walk(token) if entry!=None]
        result = ''.join(collected_results)
        return result

    def escape_html(self, raw):
        return html.escape(html.unescape(raw)).replace('&#x27;', "'")

    def escape_url(self, raw):
        """
        Escape urls to prevent code injection craziness. (Hopefully.)
        """
        return html.escape(quote(html.unescape(raw), safe='/#:()*?=%@+,&'))

    def __exit__(self, exception_type, exception_val, traceback):
        super().__exit__(exception_type, exception_val, traceback)
        html._charref = self._stdlib_charref

    def visit_to_plain(self, token):
        return self.escape_html(token.content)

    def visit_strong(self, token):
        return '<strong>'
    def leave_strong(self, token):
        return '</strong>'

    def visit_emphasis(self, token):
        return '<em>'
    def leave_emphasis(self, token):
        return '</em>'

    def visit_inline_code(self, token):
        return '<code>'
    def leave_inline_code(self, token):
        return '</code>'

    def visit_raw_text(self, token):
        return self.escape_html(token.content)

    def visit_strikethrough(self, token):
        return '<del>'
    def leave_strikethrough(self, token):
        return '</del>'

    def visit_image(self, token):
        return '<img src="{}" alt="'.format(token.src)
    def leave_image(self, token):
        if token.title:
            title = ' title="{}"'.format(self.escape_html(token.title))
        else:
            title = ''
        return '"{} />'.format(title)

    def visit_link(self, token):
        template = '<a href="{}"{}>'
        target = self.escape_url(token.target)
        if token.title:
            title = ' title="{}"'.format(self.escape_html(token.title))
        else:
            title = ''
        return template.format(target, title)
    def leave_link(self, token):
        return '</a>'

    def visit_auto_link(self, token):
        template = '<a href="{}">'
        if token.mailto:
            target = 'mailto:{}'.format(token.target)
        else:
            target = self.escape_url(token.target)
        return template.format(target)
    def leave_auto_link(self, token):
        return '</a>'

    def visit_heading(self, token):
        return '<h{}>'.format(token.level)
    def leave_heading(self, token):
        return '</h{}>'.format(token.level)

    def visit_setext_heading(self, token):
        return '<h{}>'.format(token.level)
    def leave_setext_heading(self, token):
        return '</h{}>'.format(token.level)

    def visit_quote(self, token):
        self._suppress_ptag_stack.append(False)
        return '<blockquote>'
        # elements.extend([self.render(child) for child in token.children])
    def leave_quote(self, token):
        self._suppress_ptag_stack.pop()
        return '</blockquote>'

    def visit_paragraph(self, token):
        if self._suppress_ptag_stack[-1]:
            return ''
        else:
            return '<p>'
    def leave_paragraph(self, token):
        if self._suppress_ptag_stack[-1]:
            return ''
        else:
            return '</p>'

    def visit_code_fence(self, token):
        template = '<pre><code{attr}>'
        if token.language:
            attr = ' class="{}"'.format('language-{}'.format(self.escape_html(token.language)))
        else:
            attr = ''
        return template.format(attr=attr)
    def leave_code_fence(self, token):
        return '</code></pre>'

    def visit_list(self, token):
        template = '<{}{}>\n'
        if token.start is not None:
            tag = 'ol'
            attr = ' start="{}"'.format(token.start) if token.start != 1 else ''
        else:
            tag = 'ul'
            attr = ''
        self._suppress_ptag_stack.append(not token.loose)
        return template.format(tag, attr)
    def leave_list(self, token):
        self._suppress_ptag_stack.pop()
        if token.start is not None:
            return '</ol>'
        else:
            return '</ul>'

    def visit_list_item(self, token):
        if isinstance(token.children[0], block_token.Paragraph) and not self._suppress_ptag_stack[-1]:
                return '<li>\n'
        return '<li>'
    def leave_list_item(self, token):
        return '</li>'

    def visit_table(self, token):
        result = '<table>\n'
        if token.header:
            result += '<thead>\n'
            result += self.walk(token.header)
            result += '</thead>\n'

        result += '<tbody>\n'
        return result
    def leave_table(self, token):
        return '</tbody>\n</table>\n'

    def visit_table_row(self, token):
        result = '<tr>\n'
        return result
    def leave_table_row(self, token):
        return '</tr>'

    def visit_table_cell(self, token):
        result = '<{} align="{}">'.format('td' if token.is_header else 'th', token.align or 'left') ##if token.is_header else '<td>'
        return result
    def leave_table_cell(self, token):
        return '</td>' if token.is_header else '</th>'

    def visit_html_block(self, token):
        return token.content+'\n'

    def visit_html_span(self, token):
        return token.content

    def visit_thematic_break(self, token):
        return '<hr />\n'

    def visit_line_break(self, token):
        return '\n' if token.soft else '<br />\n'