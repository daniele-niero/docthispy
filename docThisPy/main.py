import os
import io
import textwrap
import importlib
from mistletoe import HTMLRenderer, Document

from .mistletoeExtensions.wrappedSection import WrappedSection, render_wrapped_section
from .template import Templateable

DEFAULT_STYLE = os.path.normpath( '{}/../styles/default.css'.format(os.path.dirname(__file__)) )


# def registerMistletoeExtensions(renderer, extensionPaths):
#     for path in extensionPaths:
#         extension_module = importlib.import_module(path)
#         extension_module.make_extension(renderer)


# RENDERER = HTMLRenderer()
# registerMistletoeExtensions(
#     RENDERER,
#     ['docThisPy.mistletoeExtensions.wrappedSection']
# )





class HtmlPage(Templateable):
    def __init__(self):
        super().__init__('html')
        self.markcode = ''

    def append(self, markcode, dedent=True):
        if dedent:
            self.markcode += textwrap.dedent(markcode)
        else:
            self.markcode += markcode

    def toHtml(self, withTOC=False):
        if withTOC:
            self.markcode += '\n\n[TOC]'

        with HTMLRenderer() as renderer:
            print(render_wrapped_section)
            renderer.register_token(WrappedSection, render_wrapped_section)
            return renderer.render(Document(self.markcode))


    def toHtmlFile(self, outputPath, stylePath=DEFAULT_STYLE, withTOC=False):
        style = os.path.relpath(stylePath, os.path.dirname(outputPath)).replace('\\', '/')
        converted = self.toHtml(withTOC)
        result = self.templateStr.format(style, converted)

        output = io.open(outputPath, 'w')
        output.write(result)
