#! python3

import os
import click

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

@click.group(context_settings=CONTEXT_SETTINGS)
@click.option(
    '-in', '--input-path',
    type=click.Path(exists=False, dir_okay=True, file_okay=False, resolve_path=True), 
    default='../sourceDoc',
    show_default=True,
    help='main location of Commonmark files'
)
@click.option(
    '-pak', '--package-path',
    type=click.Path(exists=False, dir_okay=True, file_okay=False, resolve_path=True),
    default='.',
    show_default=True, 
    help='path to python\'s package to document'
)
@click.option(
    '-tmpl', '--templates-path',
    type=click.Path(exists=False, dir_okay=True, file_okay=False, resolve_path=True),
    default='../templates',
    show_default=True, 
    help='path to template files'
)
@click.option(
    '-out', '--output-path',
    type=click.Path(exists=False, dir_okay=True, file_okay=False, resolve_path=True),
    default='../documentation',
    show_default=True, 
    help='path where to save the result'
)
@click.pass_context
def cli(ctx, input_path, package_path, templates_path, output_path):
    ctx.obj['input-path'] = input_path
    ctx.obj['package-path'] = package_path
    ctx.obj['templates-path'] = templates_path
    ctx.obj['output-path'] = output_path

@cli.command()
@click.pass_context
@click.option('-py2', '--python2', is_flag=True, help='activate support for python 2.7')
def generate(ctx, python2):
    ''' Generate a full documentation '''
    input_path = ctx.obj['input-path']
    package_path = ctx.obj['package-path']
    templates_path = ctx.obj['templates-path']
    output_path = ctx.obj['output-path']
    
    from docThisPy.docAst.pyPackage import PyPackage
    from docThisPy.htmlGeneratorVisitor import HtmlDocVisitor

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    pack = PyPackage(package_path)

    visitor = HtmlDocVisitor(output_path)
    visitor.start(pack)




if __name__ == '__main__':
    cli(obj={})