{if node.parent:
    [{node.parent.name}]({node.parent.linkReference}.html) 
}

# {node.name}

{node.signature}  
{node.importStatement}

{if node.doc:
    :description: Description
        {node.doc}
}

{if node.hasSpecialMethods:
    :special-methods: Special Methods
        {for method in node.specialMethods:
            - {method.signature}  
              {method.brief}
        }
}
{if node.hasPublicMethods:
    :public-methods: Public Methods
        {for method in node.publicMethods:
            - {method.signature}  
              {method.brief}
        }
}
{if node.hasPrivateMethods:
    :private-methods: Private Methods
        {for method in node.privateMethods:
            - {method.signature}  
              {method.brief}
        }
}  