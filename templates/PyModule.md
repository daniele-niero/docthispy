{if node.parent:
    [{node.parent.name}]({node.parent.linkReference}.html) 
}

# {node.name}  

{if node.hasFunctions:
    :functions-list: Functions
        {for function in node.functions:
    		### {function.name}
            {if function.doc:
                {function.doc}  
            }

            ---
        }
}    


{if node.doc:
    :description: Description
        {node.doc}
}

{if node.hasAttributes:
    :attributes-list: Attributes
        {for attr in node.attributes:
            {attr.name}
            {if attr.doc:
                {attr.doc}
            }

            ---
        }
}


{if node.hasClasses:
    :classes-list: Classes
        {for class in node.classes:
            [{class.name}]({class.linkReference}.html)  
            {if class.brief:
                {class.brief}
            }

            ---
        }
}