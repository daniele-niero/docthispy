{if node.parent:
    [{node.parent.name}]({node.parent.linkReference}.html) 
}


# {node.name}  

{if node.doc:
::: Description {{description}}
{node.doc}
:::
}

{if node.hasSubPackages:
::: Sub Packages {{sub-packages}}
{for pack in node.subPackages:
    [{pack.name}]({pack.linkReference}.html)  
    {if pack.brief:
        {pack.brief}
    }

    ---
}
:::
}

{if node.hasModules:
::: Modules List {{modules-list}}
{for module in node.modules:
    [{module.name}]({module.linkReference}.html)  
    {if module.brief:
        {module.brief}  
    }
    {if module.classes:
        {for class in module.classes:
            {class.name}  
        }
    }

    ---
}
:::
}
  